###############################
#  Unity Requisites Installer #
#      V 5.3 180131 Rev15     #
#      ji5489@gmail.com       #
###############################

# Get Git credential information from user
Write-Host "Git 설정을 위해 사용자 정보(Credential)을 입력해주세요."
$gitName = Read-Host -Prompt '사용자 이름을 입력해주세요(예: 홍길동)'
$gitEmail = Read-Host -Prompt '이메일 주소를 입력해주세요(예: hong.gil.dong@naver.com)'
Write-Host "설치를 시작합니다."

# Create temporary directory for downloading
New-Item C:\DownloadTemp -Force -Type directory | Out-Null

# Test if Unity is installed or not.
$unityInstalled = $false
if(Test-Path -Path "D:\CkisLog\Unity\Editor") {
    Write-Host "기존에 설치된 Unity가 감지되었습니다. 따로 설치하지 않습니다."
    $unityInstalled = $true
}

# Unity (if not installed)

Write-Progress -Activity "[1/2] 구성요소 다운로드 중" -Status "작업 진행 중:" -PercentComplete 0;
if(!($unityInstalled)) {
    Start-Job -Name DownloadUnity -InitializationScript {Import-Module BitsTransfer} -ScriptBlock {
        Start-BitsTransfer -Source "https://download.unity3d.com/download_unity/2cac56bf7bb6/Windows32EditorInstaller/UnitySetup32-5.6.5f1.exe" -Destination "C:\DownloadTemp\UnitySetup32-5.6.5f1.exe"
    } | Out-Null
}

# VSCode
Start-Job -Name DownloadVSCode -InitializationScript {Import-Module BitsTransfer} -ScriptBlock {
    Start-BitsTransfer -Source "https://go.microsoft.com/fwlink/?LinkID=623230" -Destination "C:\DownloadTemp\VSCodeSetup-ia32.exe"
} | Out-Null

# .NET SDK
Start-Job -Name DownloadSDK -InitializationScript {Import-Module BitsTransfer} -ScriptBlock {
    Start-BitsTransfer -Source "https://download.microsoft.com/download/1/1/5/115B762D-2B41-4AF3-9A63-92D9680B9409/dotnet-sdk-2.1.4-win-x86.exe" -Destination "C:\DownloadTemp\dotnet-sdk-2.1.4-win-x86.exe"
} | Out-Null

# .NET Framework
Start-Job -Name DownloadDotNet -InitializationScript {Import-Module BitsTransfer} -ScriptBlock {
    Start-BitsTransfer -Source "https://download.microsoft.com/download/F/10/4/F942F07D-F26F-4F30-B4E3-EBD54FABA377/NDP462-KB3151800-x86-x64-AllOS-ENU.exe" -Destination "C:\DownloadTemp\NDP462-KB3151800-x86-x64-AllOS-ENU.exe"
} | Out-Null

# Git
Start-Job -Name DownloadGit -InitializationScript {Import-Module BitsTransfer} -ScriptBlock {
    Start-BitsTransfer -Source "https://www.dropbox.com/s/bsltuwl1hsj2wjp/Git-2.16.0.2-32-bit.exe?dl=1" -Destination "C:\DownloadTemp\Git-2.16.0.2-32-bit.exe"
} | Out-Null

# Fonts
Start-Job -Name DownloadFonts -InitializationScript {Import-Module BitsTransfer} -ScriptBlock {
    Start-BitsTransfer -Source "https://www.dropbox.com/s/lzm8pzypyfs4njq/Add-Font.ps1?dl=1" -Destination "C:\DownloadTemp\Add-Font.ps1"
    Start-BitsTransfer -Source "https://www.dropbox.com/s/76fq39cr1lq1vam/D2Coding-Ver1.3.1-20180115.ttf?dl=1" -Destination "C:\DownloadTemp\D2Coding-Ver1.3.1-20180115.ttf"
    Start-BitsTransfer -Source "https://www.dropbox.com/s/u0ioabe5hodwn16/D2CodingBold-Ver1.3.1-20180115.ttf?dl=1" -Destination "C:\DownloadTemp\D2CodingBold-Ver1.3.1-20180115.ttf"
} | Out-Null

# Install Fonts
Wait-Job -Name DownloadFonts | Out-Null
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[1/11] D2Coding 폰트 설치 중" -Status "작업 진행 중:" -PercentComplete 0;

Set-ExecutionPolicy Unrestricted -Force
C:\DownloadTemp\Add-Font.ps1 C:\DownloadTemp\D2CodingBold-Ver1.3.1-20180115.ttf | Out-Null
C:\DownloadTemp\Add-Font.ps1 C:\DownloadTemp\D2Coding-Ver1.3.1-20180115.ttf | Out-Null

Wait-Job -Name DownloadGit | Out-Null
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[*/10] Git 설치 중" -Status "작업 진행 중:" -PercentComplete 0;
$iGit = Start-Job -Name InstallGit -ScriptBlock {
    C:\DownloadTemp\Git-2.16.0.2-32-bit.exe /VERYSILENT /NORESTART /NOCANCEL /SP- /CLOSEAPPLICATIONS /RESTARTAPPLICATIONS /COMPONENTS="icons,ext\reg\shellhere,assoc,assoc_sh" | Out-Null
}

Wait-Job -Name DownloadVSCode | Out-Null
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[*/10] Git, VSCode 설치 중" -Status "작업 진행 중:" -PercentComplete 0;
$iCode = Start-Job -Name InstallVSCode -ScriptBlock {
    C:\DownloadTemp\VSCodeSetup-ia32.exe /VERYSILENT /MERGETASKS=!runcode | Out-Null
}

Wait-Job -Name DownloadDotNet,DownloadSDK | Out-Null
if($unityInstalled) {
    Write-Progress -Activity "[1/2] 구성요소 다운로드 중" -Status "작업 진행 중:" -Completed;
}
Write-Progress -Id 3 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[*] .NET Frameowork 및 .NET SDK 설치 중" -Status "작업 진행 중:" -PercentComplete 0;
$iNet = Start-Job -Name InstallNETFramework -ScriptBlock {
    Start-Process 'C:\DownloadTemp\NDP462-KB3151800-x86-x64-AllOS-ENU.exe' -NoNewWindow -Wait "/q /norestart"
    Start-Process 'C:\DownloadTemp\dotnet-sdk-2.1.4-win-x86.exe' -NoNewWindow -Wait "-q"
}

if(!($unityInstalled)) {
    Wait-Job -Name DownloadUnity | Out-Null
    Write-Progress -Activity "[1/2] 구성요소 다운로드 중" -Status "작업 진행 중:" -Completed;
    Write-Progress -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[*/10] Git, VSCode, Unity 설치 중" -Status "작업 진행 중:" -Completed;
    $iUnity = Start-Job -Name InstallUnity -ScriptBlock {
        Start-Process 'C:\DownloadTemp\UnitySetup32-5.6.5f1.exe' -NoNewWindow -Wait "/S /D=D:\CkisLog\Unity"
    }
}

# Setup Git Mingw32 settings
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[2/10] Git Mintty 설정 중" -Status "작업 진행 중:" -PercentComplete 5;
$minttyrc = @"
BoldAsFont=-1
Font=D2Coding
FontHeight=9
Columns=100
Rows=32
FontSmoothing=full
AllowBlinking=yes
Locale=ko_KR
Charset=UTF-8
"@
$bashrc = 'cd /c/Users/Cyber/Documents'
$minttyrc | Out-File -FilePath ~\.minttyrc -Encoding ASCII
$bashrc | Out-File -FilePath ~\.bash_profile -Encoding ASCII

# Reload EnvVar
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[3/10] 환경 변수 초기화 중" -Status "작업 진행 중:" -PercentComplete 5;
$env:Path = "%SystemRoot%\system32\WindowsPowerShell\v1.0\;C:\Windows\system32;C:\Windows;C:\Windows\System32\Wbem;C:\Windows\System32\WindowsPowerShell\v1.0\;C:\Program Files\Git\cmd;C:\Program Files\Microsoft VS Code\bin"

# Setup VSCode settings
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[4/10] VSCode 기본 테마 및 폰트 설정 중" -Status "작업 진행 중:" -PercentComplete 5;
$vscodeuser = @"
{
    "git.ignoreMissingGitWarning": true,
    "editor.fontSize": 12,
    "editor.fontFamily": "D2Coding, Consolas, Courier New, monospace",
    "workbench.startupEditor": "welcomePage",
    "terminal.integrated.shell.windows": "C:\\Windows\\System32\\WindowsPowerShell\\v1.0\\powershell.exe",
    "workbench.colorTheme": "eppz!",
    "workbench.iconTheme": "material-icon-theme"
}

"@
mkdir -Force ~\AppData\Roaming\Code\User | Out-Null
$vscodeuser | Out-File -FilePath ~\AppData\Roaming\Code\User\settings.json -Encoding UTF8

# Setup Git default credentials
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[5/10] Git 설치 완료 중" -Status "작업 진행 중:" -PercentComplete 5;
$iGit | Wait-Job | Out-Null
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[6/10] Git Credential 설정 중" -Status "작업 진행 중:" -PercentComplete 35;

git config --global user.name $gitName
git config --global user.email $gitEmail
git config --global credential.helper wincred


# Install VSCode Extensions
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[7/10] VSCode 설치 완료 중" -Status "작업 진행 중:" -PercentComplete 40;
$iCode | Wait-Job | Out-Null
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[8/10] VSCode 필요한 Extension 설치 중" -Status "작업 진행 중:" -PercentComplete 60;

code --install-extension eppz.eppz-code | Out-Null
code --install-extension PKief.material-icon-theme | Out-Null
code --install-extension ms-vscode.csharp  | Out-Null
code --install-extension Unity.unity-debug | Out-Null
code --install-extension kleber-swf.unity-code-snippets | Out-Null
code --install-extension Tobiah.unity-tools | Out-Null

# Wait for Unity installation, and if already installed -> pass.
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[9/10] Unity 설치 완료 대기중" -Status "작업 진행 중:" -PercentComplete 60;
if(!$unityInstalled) {
    $iUnity | Wait-Job | Out-Null
}

# Install Taskbar Pin
Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[10/10] 작업 표시줄에 아이콘 추가 중" -Status "작업 진행 중:" -PercentComplete 80;
$app = New-Object -c Shell.Application

$code = $app.NameSpace('C:\Program Files\Microsoft VS Code').ParseName('Code.exe')
$gitlauncher = $app.NameSpace('C:\Program Files\Git').ParseName('git-bash.exe')

$code.InvokeVerb('taskbarpin')
$gitlauncher.InvokeVerb('taskbarpin')

# Existing Editor? or New editor?
Try {
    $unity = $app.NameSpace('D:\CkisLog\Unity\Editor').ParseName('Unity.exe')
    $unity.InvokeVerb('taskbarpin')
} Catch {
    Try {
        $unity = $app.NameSpace('C:\Program Files\Unity\Editor').ParseName('Unity.exe')
        $unity.InvokeVerb('taskbarpin')
    } Catch {
        Write-Host "Unity Editor가 존재하지 않습니다!"
    }
}

Write-Progress -Id 2 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "작업 종료" -Status "작업 진행 중:" -Completed;
$iNet | Wait-Job | Out-Null
Write-Progress -Id 3 -Activity "[2/2] 구성요소 설치 중" -CurrentOperation "[*] .NET Frameowork 및 .NET SDK 설치 중" -Status "작업 진행 중:" -Completed;


Write-Host ""
Write-Host "설치 완료! 이제 Unity를 실행하여 인증 후 사용하세요."
Write-Host "Created by 정인 (ji5489@gmail.com)"