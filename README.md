# Unity Development Requisites  
Unity 개발에 필요한 구성 요소 - 스크립트 V3 업데이트 (180125)

## 1. 실행 (Powershell 상에서)
    
    powershell -nop -c "iex(New-Object Net.WebClient).DownloadString('https://gitlab.com/jungin500/unity-development-requisites/raw/master/InstallRequisites-Latest.ps1')"

이거만 하면 다운로드부터 설치까지 끝. 어때요? 참 쉽죠?
    
## 2. 레포지토리 복제

    git clone <복제할 레포지토리>
    
## 3. 이제 설치된 툴로 개발을 시작하시게나.